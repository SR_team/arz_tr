#ifndef ARZ_TR_H
#define ARZ_TR_H

#include "arz_tr_global.h"

#include <filesystem>
#include <vector>
#include <string>
#include <string_view>
#include <map>

class ARZ_TR_EXPORT Arz_tr {
public:
    Arz_tr() = default;
    Arz_tr( const std::vector<std::string> &laguages, const std::filesystem::path &path = std::filesystem::path() );

	const std::string_view tr( std::string_view text ) const;

protected:
	std::map<std::size_t /* hash */, std::string /* translate */> lang_table;
};

#endif // ARZ_TR_H
