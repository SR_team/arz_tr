#include <iostream>

#include "../Arz_tr.h"

int main()
{
    Arz_tr tr({"ru_RU", "ru_UA"});

    std::cout << tr.tr("Hello World") << std::endl;
    std::cout << tr.tr("Слава Укриане!") << std::endl;

    tr = Arz_tr({"ru_RU"}, "translations");
    std::cout << tr.tr("Hello") << std::endl;
    std::cout << tr.tr("World") << std::endl;
    return 0;
}
