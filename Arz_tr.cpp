#include "Arz_tr.h"

#include <fstream>

#include <nlohmann/json.hpp>

Arz_tr::Arz_tr( const std::vector<std::string> &laguages, const std::filesystem::path &path ) {
	for ( auto &&lang : laguages ) {
		std::ifstream file;
		if ( path.empty() )
			file.open( lang + ".json" );
		else
			file.open( path / ( lang + ".json" ) );
		if ( !file.is_open() ) continue;
		nlohmann::json json;
		file >> json;
		file.close();
		std::map<std::string, std::string> translations;
		json.get_to( translations );
		for ( auto &&[text, translation] : translations ) lang_table[std::hash<std::string>{}( text )] = translation;
	}
}

const std::string_view Arz_tr::tr( std::string_view text ) const {
	auto hash = std::hash<std::string_view>{}( text );
	if ( lang_table.contains( hash ) ) return lang_table.at( hash );
	return text;
}
